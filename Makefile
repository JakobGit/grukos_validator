MAINPY=grukos_uploader.py

.PHONY: default
default: run-py3

.PHONY: run-py2
run-py2: $(MAINPY)
	python2 $<

.PHONY: run-py3
run-py3: $(MAINPY)
	python3 $<
