#!/usr/bin/env bash
set -e

dir="$HOME/f/GKO/data/grukos/grukos/grukos_validator"

function update_to_latest_tag {
    echo "cd $1"
    cd "$1" || exit 1
    git fetch --tags
    latesttag="$(git describe --abbrev=0 --tags)"
    echo "Checking out $latesttag"
    git checkout "$latesttag"
}


[ ! -d "$dir" ] && git clone https://gitlab.com/mst-gko/grukos_validator "$dir"

update_to_latest_tag "$dir"
