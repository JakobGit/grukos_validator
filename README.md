# Grukos Validator
[![Build Status (Linux)](https://gitlab.com/mst-gko/grukos_validator/badges/master/build.svg)](https://gitlab.com/mst-gko/grukos_validator/pipelines) 
[![Build status (Windows)](https://ci.appveyor.com/api/projects/status/jju8wrsr6evgdhl9/branch/master?svg=true)](https://ci.appveyor.com/project/admesg/grukos-validator/branch/master)

This application is an interactive tool for verifying groundwater-related data 
from the local file system before it is submitted to the GRUKOS database. When 
used on computers on the MST-GKO network.

## Download
The latest stable release **v1.2.4** can be downloaded here:

- [Windows](https://ci.appveyor.com/api/projects/admesg/grukos-uploader/artifacts/grukos_validator1.0_win32.zip?tag=v1.2.4&job=Environment%3a+PYTHON%3dC%3a%5CPython36-x64)
- [Linux](https://gitlab.com/mst-gko/grukos_uploader/-/archive/v1.2.4/grukos_validator-v1.0.zip)

GRUKOS Validator also works on Mac, but we do not supply builds at present.

**Tip**: You can check if your Grukos Validator is up to date by choosing *Check 
for updates* in the *Help* menu.

## Installation
Extract the zip file downloaded through the links above into an appropriate 
folder, and execute or double-click the `grukos_uploader.exe` binary file. No 
administrative credentials are required to install or run the program.

**NOTE**: Since Grukos Validator is a new program, you may encounter anti-virus 
warnings on the first launch.

## Help
Please see the included documentation, available from the "Help" menu within 
the application, or from the [help](help/index.html) directory.

## Development
Clone this project to do active development on the code base. Start the program 
with `python grukos_validator.py` to see diagnostic output to the console. 
Whenever one or more commits are pushed to this project, the [CI 
pipeline](https://gitlab.com/mst-gko/grukos_validator/pipelines) starts the 
program and checks for superficial coding errors.
The master branch contains active development, while stable releases are marked 
as tagged commits. Tag a new release with the command:

    ./tag_new_release.sh VERSION_NUMBER

where the version number is in the format *x.y.z*, as per [semantic 
versioning](https://semver.org). The script will update all version numbers in 
the source code and the README, commit all changes, tag the commit, and push 
the release to Gitlab. The CI configuration will then build stand-alone 
releases for Linux with [Gitlab 
CI](https://gitlab.com/mst-gko/grukos_validator), and for Windows with 
[AppVeyor](https://ci.appveyor.com/project/admesg/grukos-validator). The 
download links in the README become active as soon as the pipelines finish.

The internal update mechanism in the program (*Help* > *Check for updates*) 
looks for the latest tagged release on the [Gitlab
CI](https://gitlab.com/mst-gko/grukos_validator) page, and compares it to its 
own version.

## Author and License
GRUKOS Validator is maintained by [Jakob Lanstorp](mailto:jakla@mst.dk) on the basis of the work - called GRUKOS Uploader - by former EPA employee Anders Damsgaard, and is licensed under the GNU Public License version 3 or later. See [LICENSE](LICENSE) for details.

## Changes GRUKOS Uploader to Validator
Application renamed to GRUKOS Validator since it is not an uploader. 
UI interface library changed from wx to Qt.
