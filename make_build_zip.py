#!/usr/bin/env python
# -*- coding: utf-8 -*-

import grukos_uploader
import os
import sys
import zipfile

source = '{}/dist/'.format(os.getcwd())
dest = '{}/grukos_uploader{}_{}.zip'.format(os.getcwd(),
                                            grukos_uploader.__version__,
                                            sys.platform)


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            ziph.write(file_path, file_path[len(path):])


if __name__ == '__main__':
    with zipfile.ZipFile(dest, 'w', zipfile.ZIP_DEFLATED) as outzip:
        zipdir(source, outzip)
