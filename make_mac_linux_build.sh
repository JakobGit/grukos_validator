#!/usr/bin/env bash

echo Install Python requirements
#sudo pip install pyinstaller pyshp wxPython

echo Remove previous build directory
out=dist
rm -rf "$out"
mkdir -p "$out"

echo Build stand-alone application
pyinstaller --onefile --windowed grukos_uploader.py --icon icon.ico

echo Copy auxillary files to build directory
cp LICENSE "$out"
cp icon.ico "$out"
cp -r templates "$out"
cp -r sample_data "$out"
cp -r help "$out"
echo Build complete, output located in dist/

echo Bundling build as zip file
python make_build_zip.py
