import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox, QFileDialog, QProgressDialog, QApplication, QWidget
from qtpy import QtCore

from grukos_validator_ui import Ui_MainWindow

#from PyQt5.QtWidgets import (QWidget, QPushButton, QFrame, QApplication)

class Validator(QWidget):
    def __init__(self, ui):
        super().__init__()
        self.ui = ui
        self.init_signals()

    def __enter__(self):
        return self

    def __exit__(self, ext_type, exc_value, traceback):
        pass

    def init_signals(self):
        self.ui.pushButton_close.clicked.connect(self.on_about)
        self.ui.comboBox2.currentTextChanged.connect(self.on_select_shp2)

    def on_about(self):
        print("Testing...")

        """  ------------------------------QProgressDialog -------------------------------------------------------------
        numFiles = 100000

        progress = QProgressDialog("Copying files...", "Abort Copy", 0, numFiles, None)
        progress.setMinimumWidth(400)
        progress.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.FramelessWindowHint | QtCore.Qt.CustomizeWindowHint)
        progress.setModal(True)
        progress.setMinimumDuration(0)

        for i in range(numFiles):
            progress.setValue(i)

            if progress.wasCanceled():
                break
            # ... copy one file

        progress.setValue(numFiles)
        """

        #""" ----------------------------QMessageBox.information---------------------------------------------------------
        QMessageBox.information(None, 'Short text', 'Could not open external file explore', QMessageBox.Ok)
        #"""

        """ ----------------------------QMessageBox.critical------------------------------------------------------------
        somestring = "blablatester"
        QMessageBox.critical(None, 'Error', f'Unknown operating system, {somestring} could not open external file explore', QMessageBox.Ok)
        """

        """----------------------------QFileDialog.getExistingDirectory-------------------------------------------------
        open_dir_dialog = str(QFileDialog.getExistingDirectory(None, "Choose project directory"))
        print(f'open_dir_dialog: {open_dir_dialog}')
        """

        """---------------------------QMessageBox.question--------------------------------------------------------------
        if QMessageBox.question(None, 'Please confirm?', "Current upload template is not saved! Proceed?",
                                QMessageBox.Yes | QMessageBox.No, QMessageBox.No) == QMessageBox.No:
            print("No...")
        else:
            print("Yes...")
        """


    def on_select_shp2(self, string):
        print("String %".format(string))

def window():
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)

    validator = Validator(ui)

    MainWindow.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    window()
