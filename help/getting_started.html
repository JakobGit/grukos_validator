<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
</head>
<body>
<p>
    <a href="__CLOSE__">Close</a> |
    <a href="index.html">Back to Index</a>
</p>
<hr>

<h2>Getting started</h2>
<h3>1. Outline</h3>
Grukos Uploader will scan a directory for shapefiles, and attempt to
match them against the templates supplied by MST. It will verify the
geometry type and attribute names, and inform the user of discrepancies
between templates and shapefiles.

<h3>1.1. User-interface overview</h3>
<p>
    <img src="images/grukos_uploader_main_window_annotated.png" alt="Grukos
        Uploader main window with annotations">
    <br>
<p><b>Fig. 1</b>: The Grukos Uploader main window with
    annotations</p>
<ol>
    <li>Currently selected directory on file system</li>
    <li>Press to select a directory on the file system</li>
    <li>Shapefile navigation between found shapefiles in directory</li>
    <li>Analysis of shapefile type, fields, contents, and comparison to template</li>
    <li>Navigation between MST shapefile templates</li>
    <li>Overview of template shapefiles including type and fields</li>
    <li>Overall status of all found shapefiles in the selected directory
        (1)
    </li>
</ol>
</p>


<h3>2. Selecting a project directory</h3>
<p>The first step is to select the root directory which contains all
    shapefiles to be analyzed. This directory is typically the root folder
    of a mapping project. After a directory has been chosen through the
    dialog box, the program will start recursively looking for ESRI
    Shapefiles in the directory and all sub-directories.</p>

<p><b>Tip!</b> If you do not have any shapefiles at hand, some examples
    can be loaded by selecting <tt>File &gt; Open example shapefiles</tt>.
</p>

<p><b>Another Tip!</b> It is much faster to read shapefiles from a local
    drive than a network share. It is recommended to work on a local copy if
    you want to analyze large or numerous shapefiles.</p>

<h3>3. Shapefile validation</h3>
The main purpose of Grukos Uploader is to verify if one or more
shapefiles correspond to the shapefile templates provided by MST. In
Grukos Uploader this is a two-step process. First, a corresponding
template is found automatically or manually for each shapefile.
Afterwards, the shapefile type and fields are compared against the
selected template.

<p><b>Tip!</b> Grukos Uploader compares against template shapefiles that
    are bundled with it. Press <tt>File &gt; Show shapefile templates</tt> to
    open the directory containing the actual template files.</p>

<h4>3.1. Automatic matching of shapefiles and templates</h4>
Grukos Uploader will attempt to automatically recognize what templates
the detected shapefiles are corresponding to. A match is found using the
following criteria:
</p>

<ol>
    <li>If a found shapefile has the same file name as a template, it is
        assumed to be a match.
    </li>
    <li>If the above fails, Grukos Uploader matches a shapefile against
        all templates if all attributes are contained in the template
        and they are of the same type.
    </li>
</ol>
<p>The automatic detection fails if neither of the above criteria are
    met.</p>

<h4>3.2. Manual matching of shapefiles and templates</h4>
<p>It is possible to manually select a template for comparison using the
    template drop-down (item 5 in Fig. 1).</p>

<h3>4. Navigating the results</h3>
The large text fields on the left and right (items 4 and 6 in Fig. 1) are
the places to view the type and structure of the selected shapefile and
template. Additionally, the shapefile result display (item 4 in Fig. 1)
reports how the shapefile compares against the template.

<h4>4.1. Comparison between shapefiles and templates</h4>
<p>Grukos Uploader will compare the following properties of the detected
    shapefile and a selected template:
</p>
<ol>
    <li>The encoding, e.g. utf8, latin1, or ascii</li>
    <li>The shapefile geometry type, e.g. POINT, POLYLINE, POLYGON,
        POINTZ, etc.
    </li>
    <li>The field (attribute) names. The shapefile <i>must</i> contain
        all fields of the template. Additional fields are OK, but will
        be ignored
    </li>
    <li>The field (attribute) types, e.g. text, whole number, decimal
        number, bool, date. The program will first match fields by their
        names and then compare their types
    </li>
</ol>

<p>To demonstrate how the results are presented, the following consists
    of several examples. The example shapefiles are included with
    the program (<tt>File &gt; Open example shapefiles</tt>).</p>

<h4>4.2. Example 1: Valid shapefile</h4>
<p>
    <img src="images/grukos_uploader_shapefile_navigation_ok_annotated.png"
         alt="Example comparison 1">
    <br>
<p><b>Fig. 2</b>: Correct shapefile (left) and matching template
    (right).</p>
<ol>
    <li>This field shows the file name of the shapefile</li>
    <li>The full path to the shapefile on the file system</li>
    <li>The detected encoding for the shapefile</li>
    <li>The detected geometry type for the shapefile</li>
    <li>All fields (attributes) in the shape files, including their
        <font color="blue">type</font> (in blue), length and precision.
        Precision is only relevant for floating-point numbers
    </li>
    <li>The number of records (rows) in the shapefile</li>
    <li>The file status (<font color="green">FILE OK</font> or <font
            color="red">FILE NOT OK</font>)
    </li>
</ol>
</p>

<h4>4.3. Example 2: Invalid shapefile</h4>
<p>
    <img src="images/grukos_uploader_shapefile_navigation_not_ok_annotated.png"
         alt="Example comparison 2">
    <br>
<p><b>Fig. 3</b>: Incorrect shapefile (left) and manually selected
    template (right).</p>
<ol>
    <li>The file does not correspond to the template
        (<font color="red">FILE NOT OK</font>)
    </li>
    <li>The reason for the mismatch is that one or more fields have
        incorrect types
    </li>
    <li>The field with an incorrect type is called "kortnavn"</li>
    <li>The "kortnavn" field is in the shapefile a
        <font color="blue">whole number</font></li>
    <li>However, "kortnavn" is in the template a
        <font color="blue">text</font> field
    </li>
</ol>
</p>

<h4>4.3. Example 3: Very invalid shapefile</h4>
<p>
    <img src="images/grukos_uploader_shapefile_navigation_not_ok2_annotated.png"
         alt="Example comparison 3">
    <br>
<p><b>Fig. 4</b>: Incorrect shapefile (left) and automatically
    detected template (right).</p>
<ol>
    <li>The file does not correspond to the template
        (<font color="red">FILE NOT OK</font>)
    </li>
    <li>The reasons are that the shapefile geometry type is wrong, there
        are one or more missing field names, and one or more incorrect field
        types
    </li>
    <li>The shapefile is missing a field called "koteenhed", which is
        present in the template on the right
    <li>The "lerlag" field has the wrong type (it should have been
        <font color="blue">text</font>, not <font color="blue">whole
            number</font>)
    </li>
    <li>The shapefile geometry type is POLYLINE</li>
    <li>However, the template geometry type is POLYGON</li>
</ol>
</p>

<hr>
<p>
    <a href="__CLOSE__">Close</a> |
    <a href="index.html">Back to Index</a>
</p>
</body>
</html>
