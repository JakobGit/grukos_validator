#!/usr/bin/env bash
set -e
set +H

if [ $# -lt 1 ]; then
    echo "Error: Specify version number as command line argument"
    echo "Example: ${0##/.*} 1.0.5"
    exit 1
fi
VERSION="$1"

sed -i "/host/!s/[0-9]\+\.[0-9]\+\.[0-9]\+/$VERSION/g" README.md grukos_validator.py

git diff
read -p "Does this look correct? [Y/n] " -n 1 -r
echo
if [[ $REPLY =~ ^[Nn]$ ]]; then
    exit 1
fi

git commit -a -m "Bump to version v${VERSION}"
git tag -m "Release v${VERSION}" v${VERSION}
git push
git push origin v${VERSION}
