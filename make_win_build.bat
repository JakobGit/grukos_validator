REM Run this script on a 64-bit Windows machine in a Anaconda Python 3 prompt

REM C:\GitLab\pyqt_test\env\Scripts\pyuic5.exe -x C:\GitLab\grukos_validator\grukos_validator.ui -o C:\GitLab\grukos_validator\grukos_validator_ui.py
REM C:\Anaconda3\Scripts\pyuic5.exe -x C:\GitLab\grukos_validator\grukos_validator.ui -o C:\GitLab\grukos_validator\grukos_validator_ui.py

echo Install Python requirements
pip install pyinstaller pyshp wxPython dis3 requests psycopg2

echo Remove previous build directory
rmdir /S /Q dist

echo Build stand-alone application
pyinstaller --onefile --windowed grukos_uploader.py --icon icon.ico

echo Copy auxillary files to build directory
copy LICENSE dist\
copy icon.ico dist\
md dist\templates
xcopy /S /E templates dist\templates
md dist\sample_data
xcopy /S /E sample_data dist\sample_data
md dist\help
xcopy /S /E help dist\help
echo Build complete, output located in dist/

echo Bundling build as zip file
python make_build_zip.py
